main: append.o print_and_free.o inserts.o reverse.o structs.h
	gcc -Wall -o main main.c append.o print_and_free.o inserts.o reverse.o
append.o: append.c structs.h
	gcc -c append.c
print_and_free.o: print_and_free.c structs.h
	gcc -c print_and_free.c
inserts.o: inserts.c structs.h
	gcc -c inserts.c
reverse.o: reverse.c structs.h
	gcc -c reverse.c
clean: 
	rm *.o main 
