#include <stdio.h>
#include <stdlib.h>
#include "structs.h"

void print_list(List l)
{
	//start at top of list
	if (l.head->prev == NULL)
	{
		Cell * current_cell = l.head; //create temp cell

		printf("%s\n", current_cell->word);  //print word in first cell in list
		while (current_cell->next != NULL)
		{
			current_cell = current_cell->next;  //iterate through cells in list until the bottom of list is reached
			printf("%s\n", current_cell->word);
		}
	}	
}

void free_list(List l)
{
	//start at bottom of list
	if (l.tail->next == NULL)
	{
		Cell * current_cell = l.tail;  //create temp cell

		while (current_cell->prev != NULL)
		{
			Cell * temp = current_cell->prev;
			free(current_cell->word);
			free(current_cell);
			current_cell = temp;  //iterate backwards through list until head is reached
		}
	}	
}

