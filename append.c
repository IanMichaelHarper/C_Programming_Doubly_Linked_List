#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "structs.h"

List append(char * word, List l)
{
	//start at top of list
	if (l.head == NULL)
	{
		//create new cell m and assign word
		Cell * m = malloc(sizeof(Cell));
		m->word = malloc(sizeof(char) * (strlen(word)+1));
		strcpy(m->word, word);
		
		m->prev = NULL;
		m->next = NULL;
		
		//new cell is head and tail of single celled list
		l.head = m;
		l.tail = m;
	}

	//code for rest of list
	else if (l.head != NULL)
	{
	 	//create new cell n and assign word
		Cell * n = malloc(sizeof(Cell));
		n->word = malloc(sizeof(char) * (strlen(word)+1));
		strcpy(n->word, word);

		//insert after previous cell
		n->prev = l.tail;
		n->next = NULL;

		l.tail->next = n;  //previous tail -> next = new cell
		l.tail = n; //new tail is new cell
	}
		
	return l;
}
