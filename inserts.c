#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "structs.h"

List insert_before(char* find, char* new_word, List l)
{
	//start at top of list
	if (l.head->prev == NULL)
	{
		Cell * current_cell = l.head;  // create temp cell to represent current cell in list
		while (current_cell->next != NULL)
		{
			if (strcmp(current_cell->word, find) == 0)
			{
				//create new cell
				Cell * new_cell = malloc(sizeof(Cell));
				new_cell->word = malloc(sizeof(char) * (strlen(new_word)+1));
				strcpy(new_cell->word, new_word);

				//sort pointers to insert cell before current_cell
				Cell * tmp = current_cell->prev;
				current_cell->prev = new_cell;
				new_cell->prev = tmp;
				new_cell->next = current_cell;
				(new_cell->prev)->next = new_cell;
				break;
			}
			else
			{
				current_cell = current_cell -> next;  //iterate down through list
			}
		}
	}
	return l;
}		

List insert_after (char* find, char* new_word, List l)
{
	//start at top of list
	if (l.head->prev == NULL)
	{
		Cell * current_cell = l.head;  //create temp cell to represent current cell in list
		while (current_cell->next != NULL)
		{
			if (strcmp(current_cell->word, find) == 0)
			{
				//create new cell
				Cell * new_cell = malloc(sizeof(Cell));
				new_cell->word = malloc(sizeof(char) * (strlen(new_word)+1));
				strcpy(new_cell->word, new_word);

				//sort pointers to insert new cell after current_cell
				Cell * tmp = current_cell->next;
				current_cell->next = new_cell;
				new_cell->next = tmp;
				new_cell->prev = current_cell;
				(new_cell->next)->prev = new_cell;
				break;
			}
			else
			{
				current_cell = current_cell -> next;  //iterate down through list
			}
		}
	}
	return l;
}

